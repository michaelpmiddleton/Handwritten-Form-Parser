#!/usr/bin/python

"""
    FILE:       model.py
    AUTHOR:     mmiddleton
    DATE:       22 Dec. 2016
    VERSION:    0.2.5

"""
import numpy as np
import cv2
import os
import sys
import threading
from sklearn.externals import joblib
from skimage.feature import hog


class Model:
    #
    # __init__ (): The init method called at Object instance creation.
    #
    def __init__ (self):
        self.digits = joblib.load ('digits_classifier.pkl')
        self.orig = cv2.imread ('res/filler.png')
        self.img = cv2.cvtColor (self.orig, cv2.COLOR_BGR2GRAY)
        os.system ('cls')
        return




    #
    # clear (): Clears the screen of the terminal/command line window.
    #
    def clear (self):
        os.system ('cls')
        return




    #
    # openFile (): Uses ImageMagick & Ghostscript to convert from PDF to PNG 
    #
    def scanFile (self, path):
        os.system ('convert -density 300 -sample 2550x3300 -trim "' + path + '" -alpha flatten -quality 100 "res\\outfile.png"')
        outfile = cv2.resize (cv2.imread ('res/outfile.png'), (2550, 3300))
        cv2.imwrite ('res/outfile.png', outfile)
        self.orig = cv2.imread ('res/outfile.png')
        return



    #
    # check_rotation (): Uses anchor.png to properly rotate and shift the image for maximum accuracy.
    #
    def check_rotation (self, full_degree, maxima):
        image = self.orig
    
        # For speed's sake, we store the locations and values of the maximum.
        localMaxLocs = []
        localMaxVals = []

        # Set the max bound for the for loop:
        bound = 11 if (full_degree is 1) else 10
        
        # We need this to calculate a .1 degree-precise rotation:
        for fraction in range (0, bound):
            #print ('\t' + str (full_degree) + ":\t " + str (fraction) + '/10.\n')
            
            # Set rotation:
            rotation = full_degree + (fraction * .1)
            
            # Copy the image:
            copy = image.copy ()
            cols, rows, _ = copy.shape

            # Thresh the copy: 
            _, cp_th = cv2.threshold (copy, 90, 255, cv2.THRESH_BINARY_INV)
            copy = cp_th

            # Rotate the image by (2 - rotation):
            matr = cv2.getRotationMatrix2D ((cols/2, rows/2), rotation, 1)
            rotResult = cv2.warpAffine (copy, matr, (rows, cols))
            #cv2.imwrite (('experiments/rotations/' + str (rotation) + '.png'), rotResult)

            # Test the template against the image, record the max value and location:
            template_match = cv2.matchTemplate (rotResult, self.anchor, cv2.TM_CCOEFF_NORMED)
            _, maxVal, _, maxLoc = cv2.minMaxLoc (template_match)
            template_match = None
            localMaxVals.append (maxVal)
            localMaxLocs.append (maxLoc)


        # Find the best of the localMaxima:
        localIndex = localMaxVals.index (max (localMaxVals))
        localMaxima = [(full_degree + (localIndex * .1)), localMaxVals[localIndex], localMaxLocs[localIndex]]

        # Wait for the all clear from the semaphore and append:
        self.arraySem.acquire ()
        print ('[THREAD-INFO:]\t Thread ' + str (full_degree + 3) + " DONE.")
        maxima.append (localMaxima)
        self.arraySem.release ()



    #
    # match_template (): Uses anchor.png to properly rotate and shift the image for maximum accuracy.
    #
    def match_template (self):
        # We only want one thread able to append to the maxima array, so we set up a semaphore: 
        self.arraySem = threading.Semaphore ()
        self.cvsem = threading.Semaphore ()

        # Grab the anchor and original image and thresh them so that we don't worry about the black on the corners of the images.
        self.anchor = cv2.imread ('res/anchor.png')
        _, an_th = cv2.threshold (self.anchor, 90, 255, cv2.THRESH_BINARY_INV)
        self.anchor = an_th

        # Location of the anchor on the blank file:
        LOC_X = 1245
        LOC_Y = 1513
        
        # For speed's sake, we store the locations and values of the maximum.
        maxima = []

        # Keep track of the main thread for joining later...
        master_thread = threading.currentThread ()

        # Thread the rotation process into quarters:
        print ('Processing rotation permutations for template matching...')
        for degree in range (-2, 2):
            rotationThread = threading.Thread (target = self.check_rotation, args = (degree, maxima))
            rotationThread.start ()

        # We can't move on until all of the threads have completed their job. Must join threads:
        for threadsRunning in threading.enumerate ():
            # This would cause Deadlock, so if the thread currently being observed is main, skip it.
            if threadsRunning is master_thread:
                continue
            
            threadsRunning.join ()

        # Index = Best of the 4 best MaxVals
        # Count keeps track of iteration no.
        index = 0
        count = 0

        # Go through values in maxima[c][1] and find the max value:
        for solutions in maxima:
            index = count if (solutions[1] > maxima[index][1]) else index
            count += 1
        
        # Rotate the image to the best determined rotation:
        rows, cols, _ = self.orig.shape
        rotMatrx  = cv2.getRotationMatrix2D ((cols/2, rows/2), maxima[index][0], 1)
        imgRotated = cv2.warpAffine (self.orig, rotMatrx, (cols, rows))
        #cv2.imwrite ('experiments/TRUE-ROT.png', imgRotated)

        print ('BEST RESULT FOUND: ' + str (maxima[index][0]) + ' degree(s) rotation.')

        # Shift the image to match the blank image's anchor position:
        print ('SHITFING:\t' + str (LOC_X - (maxima[index][2][0])) + ' (x-axis),\t' + str (LOC_Y - (maxima[index][2][1])) + ' (y-axis)')
        shiftMatrx = np.float32 ([[1, 0, (LOC_X - maxima[index][2][0])], [0, 1, (LOC_Y - maxima[index][2][1])]])
        self.img = cv2.warpAffine (imgRotated, shiftMatrx, (cols, rows))
        cv2.imwrite ('res/outfile.png', self.img)
        
        return



    #
    # isolate ():   Applies threshold (further writing isolation) and crops to preset bounds (based on blank file).
    #
    def isolate (self):
        print ('Applying Threshold.')
        
        # Threshold:
        ret, im_th = cv2.threshold (self.img, 90, 255, cv2.THRESH_BINARY_INV)
        #halfThresh = cv2.subtract (img, im_th)
        #thresh = cv2.add (halfThresh, im_th)
        thresh = cv2.cvtColor (im_th, cv2.COLOR_BGR2GRAY)


        # Titles/ID's of each of the coords listed in INDICES.
        titles = [
            'DEE_MM01',
            'DEE_MM02',
            'DEE_DD01',
            'DEE_DD02',
            'DEE_YY01',
            'DEE_YY02',
          
            'DFE_MM01',
            'DFE_MM02',
            'DFE_DD01',
            'DFE_DD02',
            'DFE_YY01',
            'DFE_YY02',
          
            'LWFBL_MM01',
            'LWFBL_MM02',
            'LWFBL_DD01',
            'LWFBL_DD02',
            'LWFBL_YY01',
            'LWFBL_YY02',
          
            'LWUPL_MM01',
            'LWUPL_MM02',
            'LWUPL_DD01',
            'LWUPL_DD02',
            'LWUPL_YY01',
            'LWUPL_YY02',
          
            'LWHAL_MM01',
            'LWHAL_MM02',
            'LWHAL_DD01',
            'LWHAL_DD02',
            'LWHAL_YY01',
            'LWHAL_YY02'
            ]

        # Idices of the individual boxes based on coordinates of blank document:
        indices = [
            [1306, 1538, 1412, 1621],
            [1412, 1538, 1508, 1621],
            [1596, 1538, 1689, 1621],
            [1689, 1538, 1781, 1621],
            [1867, 1538, 1960, 1621],
            [1960, 1538, 2051, 1621],
            
            [1306, 1660, 1412, 1743],
            [1412, 1660, 1507, 1743],
            [1596, 1660, 1688, 1743],
            [1688, 1660, 1780, 1743],
            [1867, 1660, 1960, 1743],
            [1960, 1660, 2050, 1743],
                     
            [1306, 1770, 1411, 1851],
            [1373, 1770, 1507, 1851],
            [1595, 1770, 1688, 1851],
            [1688, 1770, 1781, 1851],
            [1867, 1770, 1960, 1851],
            [1960, 1770, 2049, 1851],

            [1306, 1879, 1411, 1963],
            [1411, 1879, 1506, 1963],
            [1595, 1879, 1687, 1963],
            [1687, 1879, 1779, 1963],
            [1866, 1879, 1958, 1963],
            [1958, 1879, 2048, 1963],
          
            [1306, 1990, 1411, 2074],
            [1411, 1990, 1505, 2074],
            [1595, 1990, 1687, 2074],
            [1687, 1990, 1778, 2074],
            [1865, 1990, 1958, 2074],
            [1958, 1990, 2048, 2074]
            ]
       
        # Go through each of the indices, recognizing each character.
        values = []
        c = 0
        for item in indices:
            crop = thresh[(indices[c][1]):(indices[c][3]), (indices[c][0]):(indices[c][2])]

            # Make black border to prevent Bug #1:
            for swipe in range (0, 5):
                cv2.line (crop, (0+swipe, 0), (0+swipe, indices[c][3] - indices[c][1]), (0, 0, 0), 2) # LEFT
                cv2.line (crop, (0, 0+swipe), (indices[c][2]-indices[c][0], 0+swipe), (0, 0, 0), 2) # TOP
                cv2.line (crop, (indices[c][2]-indices[c][0]-swipe, 0), (indices[c][2]-indices[c][0]-swipe, indices[c][3]-indices[c][1]), (0, 0, 0), 2) # RIGHT
                cv2.line (crop, (0, indices[c][3]-indices[c][1]-swipe), (indices[c][2]-indices[c][0], indices[c][3]-indices[c][1]-swipe), (0, 0, 0), 2) # BOTTOM

              
            # Resize the image:
            crop = cv2.resize (crop, (28, 28), interpolation = cv2.INTER_AREA)
            crop = cv2.dilate (crop, (3, 3))
            #cv2.imwrite (('experiments/crops/' + titles[c] + '.png'), crop)
            
            # Calculate HOG feature match and write the answer on the image:
            crop_hog_fd = hog (crop, orientations = 9, pixels_per_cell = (14, 14), cells_per_block = (1, 1), visualise = False)
            num = self.digits.predict (np.array ([crop_hog_fd], 'float64'))
            values.extend(num)
            #print (titles[c] + '\t-\t' + str (num))

            c += 1

        return values
