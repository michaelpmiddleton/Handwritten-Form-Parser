#!/usr/bin/python

"""
    FILE:       packager.py
    AUTHOR:     mmiddleton
    DATE:       24 Jan. 2017
    VERSION:    0.2.5

"""
from cx_Freeze import setup, Executable
import scipy
import os

setup (
        name = 'ForPar',
        version = '0.2.5',
        description = 'A Handwritten Form Parser written in Python 2.7',
        options = { 'build_exe': {
            'includes': [
                'numpy',
                'numpy.core._methods',
                'numpy.lib.format',
                'client',
                'model',
                'view',
                'cv2',
                'threading',
                'sklearn.externals',
                'skimage.feature',
                'PIL',
                'Tkinter',
                'tkFileDialog',
                'json',
            ]
            }},
        executables = [Executable ('client.py')]
)
