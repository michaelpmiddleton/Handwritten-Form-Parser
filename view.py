#!/usr/bin/python

"""
FILE:       view_controller.py
AUTHOR:     mmiddleton
DATE:       6 Jan. 2017
VERSION:    0.2.5

"""
from Tkinter import *
from tkFileDialog import askopenfilename
from PIL import Image, ImageTk
import sys


class View:
    #
    # __init__ ()
    #
    def __init__ (self):
        self.window = Tk (className = 'ForPar: A Hand-Written Form Parser')
        self.menubar = None

        # These entries are for the Controller to access:
        self.patient_name_entry = None       
        self.dob_mm_entry = None
        self.dob_dd_entry = None
        self.dob_yy_entry = None
        self.hpn_entry = None
        self.patient_phone_entry = None
        self.dee_notReq_bool = None
        self.dee_mm_entry = None
        self.dee_dd_entry = None
        self.dee_yy_entry = None
        self.dfe_notReq_bool = None
        self.dfe_mm_entry = None
        self.dfe_dd_entry = None
        self.dfe_yy_entry = None
        self.lwfbl_notReq_bool = None
        self.lwfbl_mm_entry = None
        self.lwfbl_dd_entry = None
        self.lwfbl_yy_entry = None
        self.lwupl_notReq_bool = None
        self.lwupl_mm_entry = None
        self.lwupl_dd_entry = None
        self.lwupl_yy_entry = None
        self.lwhal_notReq_bool = None
        self.lwhal_mm_entry = None
        self.lwhal_dd_entry = None
        self.lwhal_yy_entry = None
        self.mrhav_entry = None
        self.mrbpv_entry = None
        self.practice_name_entry = None
        self.provider_phone_entry = None
        self.provider_name_entry = None
        self.date_complete_mm_entry = None
        self.date_complete_dd_entry = None
        self.date_complete_yy_entry = None
        self.error_label= None
        self.submit_button = None
        return



    #
    #   updateImage (): Changes the image in the window to the new OUTFILE.PNG
    #
    def updateImage (self):
        preimage = Image.open ('res/outfile.png')
        preimage = preimage.crop ((235, 1062, 2190, 2734))
        preimage = preimage.resize ((760, 890))
        img = ImageTk.PhotoImage (preimage)
        self.pp_image_label.configure (image = img)
        self.pp_image_label.image = img
        return

    #
    # updateFields ():
    #
    def updateFields (self, values):
        # Erase whatever may be in these fields:
        self.dee_mm_entry.delete (0, END)
        self.dee_dd_entry.delete (0, END)
        self.dee_yy_entry.delete (0, END)
        self.dfe_mm_entry.delete (0, END)
        self.dfe_dd_entry.delete (0, END)
        self.dfe_yy_entry.delete (0, END)
        self.lwfbl_mm_entry.delete (0, END)
        self.lwfbl_dd_entry.delete (0, END)
        self.lwfbl_yy_entry.delete (0, END)
        self.lwupl_mm_entry.delete (0, END)
        self.lwupl_dd_entry.delete (0, END)
        self.lwupl_yy_entry.delete (0, END)
        self.lwhal_mm_entry.delete (0, END)
        self.lwhal_dd_entry.delete (0, END)
        self.lwhal_yy_entry.delete (0, END)

        #Repopulate with OCR-provided values:
        self.dee_mm_entry.insert (0, (str (values[0]) + str (values[1])))
        self.dee_dd_entry.insert (0, (str (values[2]) + str (values[3])))
        self.dee_yy_entry.insert (0, (str (values[4]) + str (values[5])))
        self.dfe_mm_entry.insert (0, (str (values[6]) + str (values[7])))
        self.dfe_dd_entry.insert (0, (str (values[8]) + str (values[9])))
        self.dfe_yy_entry.insert (0, (str (values[10]) + str (values[11])))
        self.lwfbl_mm_entry.insert (0, (str (values[12]) + str (values[13])))
        self.lwfbl_dd_entry.insert (0, (str (values[14]) + str (values[15])))
        self.lwfbl_yy_entry.insert (0, (str (values[16]) + str (values[17])))
        self.lwupl_mm_entry.insert (0, (str (values[18]) + str (values[19])))
        self.lwupl_dd_entry.insert (0, (str (values[20]) + str (values[21])))
        self.lwupl_yy_entry.insert (0, (str (values[22]) + str (values[23])))
        self.lwhal_mm_entry.insert (0, (str (values[24]) + str (values[25])))
        self.lwhal_dd_entry.insert (0, (str (values[26]) + str (values[27])))
        self.lwhal_yy_entry.insert (0, (str (values[28]) + str (values[29])))

        return



    #
    #   createView ():  Creates the elements on the window.
    #
    def createView (self):
        self.window.resizable (FALSE, FALSE)

        #menubar:  The menu bar for the program.
        self.menubar = Menu (self.window)

        # menubar > controlMenu:  Control section of menubar.
        controlMenu = Menu (
            self.menubar,
            tearoff = 0
            )
        controlMenu.add_command (label = 'New File')
        #NOTE: Others to be added as needed.

        self.menubar.controlMenu = controlMenu
        self.menubar.add_cascade (
            label = 'Control',
            menu = controlMenu
            )

        self.menubar.add_command (label = 'About')
        self.window.config (menu = self.menubar)

        # prePane: PDF preview of document.
        prePane = Frame (self.window)
        pp_image = ImageTk.PhotoImage (Image.open ('res/filler.png'))
        self.pp_image_label = Label (
            prePane,
            image = pp_image
            )
        self.pp_image_label.image = pp_image
        self.pp_image_label.pack ()
        prePane.pack (
            side = LEFT,
            fill = BOTH,
            expand = 1
            )

        # dataPane: Data entry by ForPar & User.
        dataPane = Frame (self.window)
        dataPane.pack (
            side = LEFT,
            fill = BOTH,
            expand = 1
            )

        # dataPane > patientPane: Patient information section.
        patientPane_label = Label (
            dataPane,
            fg = 'white',
            bg = 'black',
            text = 'PATIENT INFORMATION:',
            font = 'Verdana 10 bold'
            )
        patientPane_label.pack (fill = X)
        patientPane = Frame (dataPane)
        patientPane.pack (fill = BOTH)


        patient_name_frame = Frame (patientPane)
        patient_name_frame.pack (
            fill = BOTH,
            expand = 1,
            side = TOP
            )
        patient_name_label  = Label (
            patient_name_frame,
            text = 'Patient Name:'
            )
        self.patient_name_entry = Entry (patient_name_frame)
        self.patient_name_entry.name = 'PATIENT NAME'
        patient_name_label.pack (
            side = LEFT,
            fill = BOTH
            )
        self.patient_name_entry.pack (
            fill = BOTH,
            expand = 1
            )

        dob_frame = Frame (patientPane)
        dob_frame.pack (
            fill = BOTH,
            expand = 1,
            side = TOP
            )
        dob_label = Label (
            dob_frame,
            text = 'Date of Birth:'
            )
        self.dob_mm_entry = Entry (
            dob_frame,
            justify = 'center'
            )
        self.dob_mm_entry.name = 'PATIENT DOB (Month)'
        self.dob_dd_entry = Entry (
            dob_frame,
            justify = 'center'
            )
        self.dob_dd_entry.name = 'PATIENT DOB (Day)'
        self.dob_yy_entry = Entry (
            dob_frame,
            justify = 'center'
            )
        self.dob_yy_entry.name = 'PATIENT DOB (Year)'
        dob_label.pack (
            side = LEFT,
            fill = BOTH
            )
        self.dob_mm_entry.pack (
            fill = BOTH,
            expand = 1,
            side = LEFT
            )
        self.dob_dd_entry.pack (
            fill = BOTH,
            expand = 1,
            side = LEFT
            )
        self.dob_yy_entry.pack (
            fill = BOTH,
            expand = 1,
            side = LEFT
            )

        hpn_frame = Frame (patientPane)
        hpn_frame.pack (
            side = TOP,
            expand = 1,
            fill = BOTH
            )
        hpn_label = Label (
            hpn_frame,
            text = 'Health Plan No.:'
            )
        self.hpn_entry = Entry (hpn_frame)
        self.hpn_entry.name = 'PATIENT HEALTH PLAN NO.'
        hpn_label.pack (
            side = LEFT,
            fill = BOTH
            )
        self.hpn_entry.pack (
            fill = BOTH,
            expand = 1
            )


        patient_phone_frame = Frame (patientPane)
        patient_phone_frame.pack (
            side = TOP,
            fill = BOTH,
            expand = 1
            )
        patient_phone_label = Label (
            patient_phone_frame,
            text = 'Phone:'
            )
        self.patient_phone_entry = Entry (patient_phone_frame)
        self.patient_phone_entry.name = "PATIENT PHONE"
        patient_phone_label.pack (
            side = LEFT,
            fill = BOTH
            )
        self.patient_phone_entry.pack (
            fill = BOTH,
            expand = 1
            )






        # dataPane > historyPane: Patient history section.
        hp_label = Label (
            dataPane,
            text = 'HISTORY',
            bg = 'black',
            fg = 'white',
            font = 'Verdana 10 bold'
            )
        hp_label.pack (fill = X)
        historyPane = Frame (dataPane)
        historyPane.pack (fill = BOTH)


        Grid.rowconfigure (
            historyPane,
            0,
            weight = 1
            )
        Grid.columnconfigure(
            historyPane,
            0,
            weight = 1
            )

        dca_label = Label (
            historyPane,
            text = 'DIABETES CARE ACTIVITY',
            bg = '#494949',
            fg = 'white',
            font = 'Verdana 10 bold'
            )
        dlc_label = Label (
            historyPane,
            text = 'DATE LAST COMPLETED',
            bg = '#494949',
            fg = 'white',
            font = 'Verdana 10 bold'
            )
        dee_label = Label (
            historyPane,
            text = 'Diabetes Eye Exam:'
            )
        dfe_label = Label (
            historyPane,
            text = 'Diabetes Foot Exam:'
            )
        lwfbl_label = Label (
            historyPane,
            text = 'Lab Work-up (Fasting Blood Lipid):'
            )
        lwupl_label = Label (
            historyPane,
            text = 'Lab Work-up (Urine/Protein Levels):'
            )
        lwhal_label = Label (
            historyPane,
            text = 'Lab Work-up (Hemoglobin A1c Levels):'
            )
        notReq_label = Label (
            historyPane,
            text = 'Not Req.',
            font = 'Verdana 8 bold'
            )
        dlc_month_label = Label (
            historyPane,
            text = 'MM',
            font = 'Verdana 8 bold'
            )
        dlc_day_label = Label (
            historyPane,
            text = 'DD',
            font = 'Verdana 8 bold'
            )
        dlc_year_label = Label (
            historyPane,
            text = 'YY',
            font = 'Verdana 8 bold'
            )
        mrhav_label = Label (
            historyPane,
            text = 'Most Recent Hemoglobin A1c Value:'
            )
        mrbpv_label = Label (
            historyPane,
            text = 'Most Recent Blood Preasure Value:'
            )
        self.dee_notReq_bool = BooleanVar ()
        self.dfe_notReq_bool = BooleanVar ()
        self.lwfbl_notReq_bool = BooleanVar ()
        self.lwupl_notReq_bool = BooleanVar ()
        self.lwhal_notReq_bool = BooleanVar ()
        dee_notReq_checkbutton = Checkbutton (
            historyPane,
            variable = self.dee_notReq_bool
            )
        dfe_notReq_checkbutton = Checkbutton (
            historyPane,
            variable = self.dfe_notReq_bool
            )
        lwfbl_notReq_checkbutton = Checkbutton (
            historyPane,
            variable = self.lwfbl_notReq_bool
            )
        lwupl_notReq_checkbutton = Checkbutton (
            historyPane,
            variable = self.lwupl_notReq_bool
            )
        lwhal_notReq_checkbutton = Checkbutton (
            historyPane,
            variable = self.lwhal_notReq_bool
            )
        self.dee_mm_entry = Entry (
            historyPane,
            justify = 'center'
            )
        self.dee_mm_entry.name = 'DIAB. EYE EXAM DATE (Month)'
        self.dee_dd_entry = Entry (
            historyPane,
            justify = 'center'
            )
        self.dee_dd_entry.name = 'DIAB. EYE EXAM DATE (Day)'
        self.dee_yy_entry = Entry (
            historyPane,
            justify = 'center'
            )
        self.dee_yy_entry.name = 'DIAB. EYE EXAM DATE (Year)'
        self.dfe_mm_entry = Entry (
            historyPane,
            justify = 'center'
            )
        self.dfe_mm_entry.name = 'DIAB. FOOT EXAM DATE (Month)'
        self.dfe_dd_entry = Entry (
            historyPane,
            justify = 'center'
            )
        self.dfe_dd_entry.name = 'DIAB. FOOT EXAM DATE (Day)'
        self.dfe_yy_entry = Entry (
            historyPane,
            justify = 'center'
            )
        self.dfe_yy_entry.name = 'DIAB. FOOT EXAM DATE (Year)'
        self.lwfbl_mm_entry = Entry (
            historyPane,
            justify = 'center'
            )
        self.lwfbl_mm_entry.name = 'LAB - FASTING BLOOD LIP. (Month)'
        self.lwfbl_dd_entry = Entry (
            historyPane,
            justify = 'center'
            )
        self.lwfbl_dd_entry.name = 'LAB - FASTING BLOOD LIP. (Day)'
        self.lwfbl_yy_entry = Entry (
            historyPane,
            justify = 'center'
            )
        self.lwfbl_yy_entry.name = 'LAB - FASTING BLOOD LIP. (Year)'
        self.lwupl_mm_entry = Entry (
            historyPane,
            justify = 'center'
            )
        self.lwupl_mm_entry.name = 'LAB - URINE/PROTEIN LVL. (Month)'
        self.lwupl_dd_entry = Entry (
            historyPane,
            justify = 'center'
            )
        self.lwupl_dd_entry.name = 'LAB - URINE/PROTEIN LVL. (Day)'
        self.lwupl_yy_entry = Entry (
            historyPane,
            justify = 'center'
            )
        self.lwupl_yy_entry.name = 'LAB - URINE/PROTEIN LVL. (Year)'
        self.lwhal_mm_entry = Entry (
            historyPane,
            justify = 'center'
            )
        self.lwhal_mm_entry.name = 'LAB - HEMOGLOBIN A1c LVL. (Month)'
        self.lwhal_dd_entry = Entry (
            historyPane,
            justify = 'center'
            )
        self.lwhal_dd_entry.name = 'LAB - HEMOGLOBIN A1c LVL. (Day)'
        self.lwhal_yy_entry = Entry (
            historyPane,
            justify = 'center'
            )
        self.lwhal_yy_entry.name = 'LAB - HEMOGLOBIN A1c LVL. (Year)'
        self.mrhav_entry = Entry (historyPane)
        self.mrhav_entry.name = 'MOST RECENT HEMOGLOB. A1c LVL.'
        self.mrbpv_entry = Entry (historyPane)
        self.mrbpv_entry.name = 'MOST RECENT BLOOD PRESS. VAL.'


        dca_label.grid (
            columnspan = 2,
            row = 0,
            column = 0,
            sticky = 'nsew'
            )
        dlc_label.grid (
            columnspan = 3,
            row = 0,
            column = 2,
            sticky = 'nsew'
            )
        notReq_label.grid (
            row = 1,
            column = 1,
            sticky = 'nsew'
            )
        dlc_month_label.grid (
            row = 1,
            column = 2
            )
        dlc_day_label.grid (
            row = 1,
            column = 3
            )
        dlc_year_label.grid (
            row = 1,
            column = 4
            )
        dee_label.grid (
            row = 2,
            column = 0,
            sticky = 'w'
            )
        dee_notReq_checkbutton.grid (
            row = 2,
            column = 1
            )
        self.dee_mm_entry.grid (
            row = 2,
            column = 2
            )
        self.dee_dd_entry.grid (
            row = 2,
            column = 3
            )
        self.dee_yy_entry.grid (
            row = 2,
            column = 4
            )
        dfe_label.grid (
            row = 3,
            column = 0,
            sticky = 'w'
            )
        dfe_notReq_checkbutton.grid (
            row = 3,
            column = 1
            )
        self.dfe_mm_entry.grid (
            row = 3,
            column = 2
            )
        self.dfe_dd_entry.grid (
            row = 3,
            column = 3
            )
        self.dfe_yy_entry.grid (
            row = 3,
            column = 4
            )
        lwfbl_label.grid (
            row = 4,
            column = 0,
            sticky = 'w'
            )
        lwfbl_notReq_checkbutton.grid (
            row = 4,
            column = 1
            )
        self.lwfbl_mm_entry.grid (
            row = 4,
            column = 2
            )
        self.lwfbl_dd_entry.grid (
            row = 4,
            column = 3
            )
        self.lwfbl_yy_entry.grid (
            row = 4,
            column = 4
            )
        lwupl_label.grid (
            row = 5,
            column = 0,
            sticky = 'w'
            )
        lwupl_notReq_checkbutton.grid (
            row = 5,
            column = 1
            )
        self.lwupl_mm_entry.grid (
            row = 5,
            column = 2
            )
        self.lwupl_dd_entry.grid (
            row = 5,
            column = 3
            )
        self.lwupl_yy_entry.grid (
            row = 5,
            column = 4
            )
        lwhal_label.grid (
            row = 6,
            column = 0,
            sticky = 'w'
            )
        lwhal_notReq_checkbutton.grid (
            row = 6,
            column = 1
            )
        self.lwhal_mm_entry.grid (
            row = 6,
            column = 2
            )
        self.lwhal_dd_entry.grid (
            row = 6,
            column = 3
            )
        self.lwhal_yy_entry.grid (
            row = 6,
            column = 4
            )
        mrhav_label.grid (
            row = 7,
            column = 0,
            stick = 'w'
            )
        self.mrhav_entry.grid (
            row = 7,
            column = 1
            )
        mrbpv_label.grid (
            row = 7,
            column = 3,
            sticky = 'e'
            )
        self.mrbpv_entry.grid (
            row = 7,
            column = 4,
            )




        # dataPane > providerPane: Provider information section.
        providerPane_label = Label (
            dataPane,
            text = 'PROVIDER INFORMATION:',
            bg = 'black',
            fg = 'white',
            font = 'Verdana 10 bold'
            )
        providerPane_label.pack (fill = X)
        providerPane = Frame (dataPane)
        providerPane.pack (
            fill = X,
            )


        practice_name_frame = Frame (providerPane)
        practice_name_frame.pack (
            fill = X,
            expand = 1,
            side = TOP
            )
        practice_name_label = Label (
            practice_name_frame,
            text = 'Practice Name:',
            font = 'Verdana 10 bold'
            )
        self.practice_name_entry = Entry (practice_name_frame)
        self.practice_name_entry.name = 'PRACTICE NAME'
        practice_name_label.pack (
            side = LEFT,
            fill = X
            )
        self.practice_name_entry.pack (
            fill = X,
            expand = 1
            )


        provider_phone_frame = Frame (providerPane)
        provider_phone_frame.pack (
            fill = BOTH,
            expand = 1,
            side = TOP
            )
        provider_phone_label = Label (
            provider_phone_frame,
            text = 'Provider Phone:',
            font = 'Verdana 10 bold'
            )
        self.provider_phone_entry = Entry (provider_phone_frame)
        self.provider_phone_entry.name = 'PROVIDER PHONE'
        provider_phone_label.pack (
            side = LEFT,
            fill = X
            )
        self.provider_phone_entry.pack (
            fill = X,
            expand = 1
            )


        provider_name_frame = Frame (providerPane)
        provider_name_frame.pack (
            fill = BOTH,
            expand = 1,
            side = TOP
            )
        provider_name_label = Label (
            provider_name_frame,
            text = 'Provider Name:',
            font = 'Verdana 10 bold'
            )
        self.provider_name_entry = Entry (provider_name_frame)
        self.provider_name_entry.name = 'PROVIDER NAME'
        provider_name_label.pack (
            side = LEFT,
            fill = X
            )
        self.provider_name_entry.pack (
            fill = X,
            expand = 1
            )


        date_complete_frame = Frame (providerPane)
        date_complete_frame.pack (
            fill = BOTH,
            expand = 1,
            side = TOP
            )
        date_complete_label = Label (
            date_complete_frame,
            text = 'Date Completed:',
            font = 'Verdana 10 bold'
            )
        self.date_complete_mm_entry = Entry (
            date_complete_frame,
            justify = 'center'
            )
        self.date_complete_mm_entry.name = 'DATE COMPLETED (Month)'
        self.date_complete_dd_entry = Entry (
            date_complete_frame,
            justify = 'center'
            )
        self.date_complete_dd_entry.name = 'DATE COMPLETED (Day)'
        self.date_complete_yy_entry = Entry (
            date_complete_frame,
            justify = 'center'
            )
        self.date_complete_yy_entry.name = 'DATE COMPLETED (Year)'
        date_complete_label.pack (
            side = LEFT,
            fill = X
            )
        self.date_complete_mm_entry.pack (
            fill = X,
            side = LEFT,
            expand = 1
            )
        self.date_complete_dd_entry.pack (
            fill = X,
            side = LEFT,
            expand = 1
            )
        self.date_complete_yy_entry.pack (
            fill = X,
            side = LEFT,
            expand = 1
            )


        # dataPane > errorPane: Displays error information with the form.
        errorPane_label = Label (
            dataPane,
            text = 'SUBMISSION STATUS:\n(This will not be included in the database entry.)',
            bg = 'black',
            fg = 'white',
            font = 'Verdana 10 bold'
            )
        errorPane_label.pack (fill = X)
        errorPane = Frame (dataPane)
        errorPane.pack (
            fill = BOTH,
            expand = 1
            )
        self.error_label = Label (
            errorPane,
            fg = 'red',
            justify = LEFT,
            font = ('Courier New', 12)
            )
        self.error_label.pack (
            fill = Y,
            expand = 1
            )

        '''
        LOGIC: On click of 'Upload to Database,' document will verify contents
        ensure data is properly entered and, if not, will call
        collectErrors (), which will update the list and properly format
        the ERR_MSG string for this canvas.
        ''' 

        # dataPane > submit:  This button starts the verification and submission to the AHS db.
        self.submit_button = Button (
            dataPane,
            text = 'Verify & Submit',
            bg = '#13b571',
            fg = 'white',
            font = 'Verdana 12 bold',
            justify = 'center'
            )
        self.submit_button.pack (
            fill = BOTH,
            expand = 1
            )
        return
