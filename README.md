# Handwritten-Form Parser

### NOTE: As of 3 March 2017, development on this repository has ended. ###

ForPar (Project Vel'koz) is a Windows based, handwritten-form parser that was developed during my time as an intern at Abacus Health Solutions in Cranston, RI. It takes a scanned PDF image, converts it to a PNG file via Ghostscript, and removes a blank template from the filled-image. It then runs OCR algorithms from OpenCV against targeted sections of the image to recognize dates, phone numbers, and ID numbers. It compiles these data points into a JSON document that can then be sent to an AHS database.<br>
<br>
![Screenshot of ForPar](https://github.com/michaelpmiddleton/Handwritten-Form-Parser/blob/master/design/screenshot.png/)
<br><br><br>
## Credit/Sources:
### Bikramjot Singh Hanzra:
I _could not_ have gotten anywhere without Mr. Hanzra's blog post. His explanations are amazing and I highly recommend you look at his post, either by [clicking here](http://hanzratech.in/2015/02/24/handwritten-digit-recognition-using-opencv-sklearn-and-python.html) or by viewing the PDF of his post in the 'sources' folder of this project.  

### Sentdex:
This guy is a great resource for anyone looking into OpenCV for Python. Here is a link to his [OpenCV playlist](https://www.youtube.com/watch?v=Z78zbnLlPUA&list=PLQVvvaa0QuDdttJXlLtAJxJetJcqmqlQq).
<br><br><br>
## _Want to contribute/fork?_
Go right ahead! If you find any bugs or issues, please post in the issues section! If you do want to fork or use this software, I ask that you please keep my name & copyright in the source code. Thanks in advance and happy hacking! 
<br><br><br>
## _Got Questions?_
Please feel free to [email me](mailto:mp.middleton@outlook.com) with questions!
