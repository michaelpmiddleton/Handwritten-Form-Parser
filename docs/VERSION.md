# Version History
---
### 0.1:&nbsp;&nbsp;&nbsp;(26 Dec. 2016)
* Created basic logic behind MODEL.

### 0.2:&nbsp;&nbsp;&nbsp;(6 Jan. 2017)
* Bugfix 1. (Bugs: #1)
* Created basic logic behind VIEW-CONTROLLER.

### 0.2.1:&nbsp;&nbsp;&nbsp;(10 Jan. 2017)
* GUI design completed.

### 0.2.2:&nbsp;&nbsp;&nbsp;(11 Jan. 2017)
* Bugfix 2. (Bugs: #2)
* Modualized the MODEL.

### 0.2.3:&nbsp;&nbsp;&nbsp;(12 Jan. 2017)
* MVC structure created. (VIEW-CONTROLLER split into VIEW and CONTROLLER)
* MVC has initial linkage.
* Client is now the way to launch the program, not VIEW-CONTROLLER.

### 0.2.4:&nbsp;&nbsp;&nbsp;(13 Jan. 2017)
* ~~Document centers based on position of the position of the "DATE LAST COMPLETED" box.~~
* Document centers based on position of the overall dates box. (*As of 14 Feb. 2017*)
* PACKAGER created.
* OCR configured.
* MVC secondary linkage (post-document scan).

### 0.2.5:&nbsp;&nbsp;&nbsp;(21 Feb. 2017)  
* Implemented multi-threading to boost speed of template matching.
* Created the JSON object to be passed to the AHS database.