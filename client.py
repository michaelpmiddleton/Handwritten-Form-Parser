#!/usr/bin/python

"""
    FILE:       client.py
    AUTHOR:     mmiddleton
    DATE:       11 Jan. 2017
    VERSION:    0.2.5

"""
from model import Model
from view import View
from controller import Controller

class Client:
    #
    # __init__ ():  Creates each part of the MVC and attaches to the Client.
    #
    def __init__ (self):
        self.model = Model ()
        self.view = View ()
        self.controller = Controller (self.model, self.view)
        self.view.createView ()
        return


    #
    # linkAll (): Connects the MVC components together.
    #
    def linkAll (self):
        self.controller.link ()
        return




    #
    # launch (): Call the main loop of the VC.
    #
    def launch (self):
        self.view.window.mainloop ()
        return


FORPAR = Client ()
FORPAR.linkAll ()
FORPAR.launch ()
