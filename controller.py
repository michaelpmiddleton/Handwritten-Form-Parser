#!/usr/bin/python

"""
    FILE:       controller.py
    AUTHOR:     mmiddleton
    DATE:       12 Jan. 2017
    VERSION:    0.2.5

"""
import tkFileDialog
import json

class Controller:
    #
    # __init__ (): Called on object intance creation:
    #
    def __init__ (self, model, view):
        self.model = model
        self.view = view
        self.error_msg = []
        return



    #
    # link (): Connects the button from the VIEW to *this* class.
    #
    def link (self):
        self.view.submit_button.configure (command = self.submit)
        self.view.menubar.controlMenu.entryconfigure (1, command = self.loadFile)
        return



    #
    #   loadFile (): Opens a user-selected file, runs OCR on it and updates the VIEW.
    #
    def loadFile (self):
        infile = tkFileDialog.askopenfilename ()
        if (infile is not None and infile is not ''):
            print ('LOADING FILE...')
            self.model.scanFile (infile)
            self.model.match_template ()
            self.view.updateImage ()
            # TODO: AS OF 23 Feb. 2017      -   Remember to turn this back on!
            values = self.model.isolate ()
            self.view.updateFields (values)
            print ('DONE!')

        return



    #
    # submit (): Calls verify and, if True, stores as a JSON file which is then passed to AHS DB.
    #
    def submit (self):
        result = self.verify ()

        if (result):
            self.view.error_label.configure (fg = 'green')
            self.view.error_label.configure (text = 'Sumbission Complete!')
            dataToBePassed = self.jsonify ()

            '''
                ATTENTION ABACUS DEVELOPER:
                Please implement the database connection here.
            '''

        else:
            message = ''
            for error in self.error_msg:
                message += ('\n' + error)

            self.view.error_label.configure (fg = 'red')
            self.view.error_label.configure (text = message)
        
        return


    #
    # verify (): Ensures that the data in the fields is populated and is valid.
    #
    def verify (self):
        self.error_msg = []
        to_be_returned = True
        entry_data = None
        count = 0

        req_date_entries = [
                self.view.dob_mm_entry,
                self.view.dob_dd_entry,                
                self.view.dob_yy_entry,
                self.view.date_complete_mm_entry,
                self.view.date_complete_dd_entry,
                self.view.date_complete_yy_entry
                ]

        date_entries = [
                self.view.dee_mm_entry,
                self.view.dee_dd_entry,
                self.view.dee_yy_entry,
                self.view.dfe_mm_entry,
                self.view.dfe_dd_entry,
                self.view.dfe_yy_entry,
                self.view.lwfbl_mm_entry,
                self.view.lwfbl_dd_entry,
                self.view.lwfbl_yy_entry,
                self.view.lwupl_mm_entry,
                self.view.lwupl_dd_entry,
                self.view.lwupl_yy_entry,
                self.view.lwhal_mm_entry,
                self.view.lwhal_dd_entry,
                self.view.lwhal_yy_entry
                ]

        text_entries = [
                self.view.patient_name_entry,
                self.view.hpn_entry,
                self.view.patient_phone_entry,
                self.view.mrhav_entry,
                self.view.mrbpv_entry,
                self.view.practice_name_entry,
                self.view.provider_phone_entry,
                self.view.provider_name_entry
                ]


        # These dates (DOB & Date Complete) CANNOT be empty.
        for rd_entry in req_date_entries:
            if (rd_entry.get () == ''):
                to_be_returned = False
                self.error_msg.append (rd_entry.name + ' has invalid input.')
                
            else:
                try:
                    val = int (rd_entry.get ())

                    # Month:
                    if (count % 3 == 0):
                        if (val > 12):
                            to_be_returned = False
                            self.error_msg.append (rd_entry.name + ' has invalid input.')
                            continue

                    # Day:
                    if (count % 3 == 1):
                        if (val > 31):
                            to_be_returned = False
                            self.error_msg.append (rd_entry.name + ' has invalid input.')
                            continue

                except:
                    to_be_returned = False
                    self.error_msg.append (rd_entry.name + ' has invalid input.')
                    continue
            
            count += 1


        # The History Section dates may well be empty. Reset count for use in date verification.
        count = 0
        for d_entry in date_entries:
            if (d_entry.get () is not ''):
                try:
                    val = int (d_entry.get ())

                    # Month:
                    if (count % 3 == 0):
                        if (val > 12):
                            to_be_returned = False
                            self.error_msg.append (d_entry.name + ' has invalid input.')
                            continue

                    # Day:
                    if (count % 3 == 1):
                        if (val > 31):
                            to_be_returned = False
                            self.error_msg.append (d_entry.name + ' has invalid input.')
                            continue

                except:
                    to_be_returned = False
                    self.error_msg.append (d_entry.name + ' has invalid input.')
                    continue
            
            count += 1


        # The text entries CANNOT be empty. (MRHAV & MRBPV are considered Text values for simplicities sake.)
        for t_entry in text_entries:
            if (t_entry.get () is ''):
                to_be_returned = False
                self.error_msg.append (t_entry.name + ' is empty!!!')

        return to_be_returned



    #
    # jsonify ():   Takes the data in the various fields and adds them to a JSON file which is returned:
    #
    def jsonify (self):
        # Create a dictionaries to be added to the main one:
        required = 'n' if self.view.dee_notReq_bool.get() else 'y'  
        DCA_DEE = {
            'REQUIRED': required,
            'MONTH': self.view.dee_mm_entry.get (),
            'DATE': self.view.dee_mm_entry.get (),
            'YEAR': self.view.dee_yy_entry.get ()
        }

        required = 'n' if self.view.dfe_notReq_bool.get() else 'y'
        DCA_DFE = {
            'REQUIRED': required,
            'MONTH': self.view.dfe_mm_entry.get (),
            'DATE': self.view.dfe_mm_entry.get (),
            'YEAR': self.view.dfe_yy_entry.get ()
        }

        required = 'n' if self.view.lwfbl_notReq_bool.get() else 'y'
        DCA_LWFBL = {
            'REQUIRED': required,
            'MONTH': self.view.lwfbl_mm_entry.get (),
            'DATE': self.view.lwfbl_mm_entry.get (),
            'YEAR': self.view.lwfbl_yy_entry.get ()
        }

        required = 'n' if self.view.lwupl_notReq_bool.get() else 'y'
        DCA_LWUPL = {
            'REQUIRED': required,
            'MONTH': self.view.lwupl_mm_entry.get (),
            'DATE': self.view.lwupl_dd_entry.get (),
            'YEAR': self.view.lwupl_yy_entry.get ()
        }

        required = 'n' if self.view.lwhal_notReq_bool.get() else 'y'
        DCA_LWHAL = {
            'REQUIRED': required,
            'MONTH': self.view.lwhal_mm_entry.get (),
            'DATE': self.view.lwhal_dd_entry.get (),
            'YEAR': self.view.lwhal_yy_entry.get ()
        }
        
        # Create main dictionary obj to be returned as a JSON file:
        date_completed = (str (self.view.date_complete_yy_entry.get ()) + '/' + str (self.view.date_complete_dd_entry.get ()) + '/' + str (self.view.date_complete_yy_entry.get ()))
        toBeReturned = {
            'PATIENT NAME': self.view.patient_name_entry.get (),
            'DATE OF BIRTH': (str (self.view.dob_mm_entry.get ()) + '/' + str (self.view.dob_dd_entry.get ()) + '/' + str (self.view.dob_yy_entry.get ())),
            'HEALTH PLAN ID NO.': self.view.hpn_entry.get (),
            'TELEPHONE': self.view.patient_phone_entry.get (),
            'DIABETES EYE EXAM': DCA_DEE,
            'DIABETES FOOT EXAM': DCA_DFE,  
            'LAB WORK-UP OF FASTING BLOOD LIPID': DCA_LWFBL,
            'LAB WORK-UP OF URINE/PROTEIN LEVELS': DCA_LWUPL,
            'LAB WORK-UP OF HEMOGLOBIN A1c LEVELS': DCA_LWHAL,
            'PRACTICE NAME': self.view.practice_name_entry.get (),
            'PROVIDER TELEPHONE': self.view.provider_phone_entry.get (),
            'PROVIDER NAME': self.view.provider_name_entry.get (),
            'DATE COMPLETED': date_completed
            }

        return json.dumps (toBeReturned)